# Devops

Projeto criado para teste de conhecimento da disciplina de devops do curso de pós-graduação em ti do imd/ufrn

O projeto em si é  simples mas demonstra o conhecimento geral da disciplina 

## Tecnologias

 - Node.js
 - Express.js
 - Eslint
 - Cypress
 - Docker
 - Kubernetes

## Detalhes

- node.js/express: foi criado um sistema backend simples, que mostra um hello world na porta 3000.
para rodar basta abrir o terminal na raiz do aquivo e executar a seguinte linha:
"npm start"

- eslint: foi utilizado para para fazer algumas verificações no codigo como: verificaa se ta usando import ou require e verifica se ta usando aspas simples ou duplas.

- cypress: foi utilizado para fazer testes, ele testa se na porta 3000 esta aparecendo o texto "Hello world!".
para executar o cypress entre no terminal na pasta raiz do projeto e execute o seguinte comando:
"npm test"

- docker: foi utilizado o docker para criar uma imagem do projeto e para subir no docker hub

- Kubernets: foi utilizado o kubernets no google cloud para fazer deploy do projeto, para conectar o docker com o gitlab ci/cd foi feito uma instalação do agente utilizando o helm(ver documentação )

## Conclusão 

foi criado um pipeline com jobs para cada etapa, onde todos os jobs tiveram exito ao serem executados.
