FROM node:19-alpine3.15
WORKDIR /usr/src/app
COPY . ./
RUN npm ci --production
EXPOSE 3000
CMD ["npm", "start"]